package com.company;

import java.util.Map;

public interface CountMap<K> {
    // добавляет элемент в этот контейнер
    void add(K o);

    // возвращает кол-во добавлений данного элемента
    int getCount(K o);

    // удаляет элемент из контейнера и возвращает количество его добавлений (до удаления)
    int remove(K o);

    // кол-во разных элементов
    int size();

    // добавить все элементы из source  в текцщий котейнер , при совпадении ключей, суммировать значения
    void addAll(CountMap<K> source);

    // вернуть java.util.Map . ключ - добавленный элемент, значение - количество его добавлений
    Map toMap();

    // тот же самый контракт как toMap() , только всю информацию записать в destination
    void toMap(Map<K,Integer> destination);

}
