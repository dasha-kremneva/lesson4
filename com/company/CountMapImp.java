package com.company;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CountMapImp<K> implements CountMap<K> {
    // ключи - это Т , а значения  - это int ключей

    Set <Pair<K>> setPair;

    public CountMapImp(){
        this.setPair =  new HashSet <> ();
    }

    @Override
    public void add(K obj) {
        if (!setPair.isEmpty()) {
            int pr = 0;
            for (Pair<K> pair : setPair) {
                if (pair.getKey().equals(obj)){
                    pair.setValue(pair.getValue() + 1);
                    pr = 1;
                }
            }
            if(pr == 0)
                setPair.add(new Pair<>(obj, 1));
        }
        else
            setPair.add(new Pair<>(obj, 1));
    }


    @Override
    public int getCount(K obj) {
             for (Pair<K> pair : setPair) {
            if (pair.getKey().equals(obj))
                 return  pair.getValue();
        }
        return 0;
    }

    @Override
    public int remove(K obj){
        for (Pair<K> pair : setPair) {
            if (pair.getKey().equals(obj)) {
                setPair.remove(pair);
                return pair.getValue();
            }
        }
        return 0;
    }

    @Override
    public int size() {
        return setPair.size();
    }


    @Override
    public void addAll(CountMap<K> countMapImp) {
        for (Pair<K> pair : setPair) {
            int count1 = pair.getValue();
            int count2 = countMapImp.getCount(pair.getKey());
            if (count2 != 0) {
                count1 += count2;
                pair.setValue(count1);
            } else {
                setPair.add(new Pair<>(pair.getKey(), count1));
            }
        }
    }

    @Override
    public Map<K, Integer> toMap() {
        Map<K, Integer> map = new HashMap<>();
        for (Pair<K> pair: setPair) {
            map.put(pair.getKey(),pair.getValue());
        }
        return map;
    }

    @Override
    public void toMap(Map<K, Integer> destination) {
        destination = new HashMap<>();
        for (Pair<K> pair: setPair) {
            destination.put(pair.getKey(),pair.getValue());
        }
    }
}
