package com.company;

public class Main {

    public static void main(String[] args) {
        CountMap<Integer> map = new CountMapImp<>();

        map.add(3);
        map.add(3);
        map.add(3);
        map.add(3);

        map.add(5);
        map.add(5);
        map.add(5);

        map.add(1);
        map.add(1);
        map.add(1);


        System.out.println("3 = " + map.getCount(3));
        System.out.println("5 = " + map.getCount(5));
        System.out.println("1 = " + map.getCount(1));

    }
}
